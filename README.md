happydev2014workshop
====================


##Requirements:##

- node.js v0.10.*
- npm (last stable)

##Node modules:##
- browserify: ^6.3.3
- chai: ^1.10.0
- express: ^4.10.4
- istanbul: ^0.3.2
- jquery-browserify: ^1.8.1
- lodash: ^2.4.1
- mocha: ^2.0.1
- mocha-istanbul: ^0.2.0
- mocha-phantomjs: ^3.5.1
- mustache-express: ^1.2.0
- phantomjs: ^1.9.12
- sinon: ^1.12.1
- stylus: ^0.49.3


##Install from repo:##

	git clone https://rabbiabram@bitbucket.org/rabbiabram/happydev2014workshop.git
	
	npm install




##Vagrant box:##

	vagrant box add ./happydev2014fullstack.box --name happydev2014/fullstack
	
	cd $project_dir && vagrant init happydev2014/fullstack
	
	vagrant ssh
	
	./moveapp.sh
	
	node index.js
