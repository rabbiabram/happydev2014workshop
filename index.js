var express = require('express');
var path = require('path');

var app = express();

app.use('/static', express.static(path.join(__dirname, 'static')));

var mustacheExpress = require('mustache-express');

app.engine('mustache', mustacheExpress());

app.set('view engine', 'mustache');

app.set('views', path.join(__dirname, 'views'));


app.set('static', path.join(__dirname, 'static'));



app.get('/', function(req, res) {
	app.render('index', function(err, html) {
		if (err) {
			res.send(err);
			return console.error(err);
		}
		res.send(html);	
	})
});

var server = app.listen(3000, function() {
	var host = server.address().address;
	var port = server.address().port;

	console.log(host, port);

});