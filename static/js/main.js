var $ = require('jquery-browserify');
var chai = require('chai');
var sinon = require('sinon');
var chai$ = require('chai-jquery');

chai.use(chai$);

$(function docReady() {
    mocha.setup('bdd');
    mocha.bail(false);

    var expect = chai.expect;


    describe('body', function() {
        describe('header', function() {
            var $header = $('.b-header');
            it('should be exist', function() {
                expect($header).to.be.exist;
            });
            it('should be visible', function() {
                expect($header).to.be.visible;
            });
            it('should have height equal 40px', function() {
                expect($header).to.have.css('height', '40px');
            });
        });
    });

    if (window.mochaPhantomJS) { mochaPhantomJS.run(); }
    else { mocha.run(); }
});